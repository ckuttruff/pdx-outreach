root = "#{Dir.getwd}"

if ENV['RACK_ENV'] == 'production'
  bind 'tcp://localhost:9000'
  pidfile "#{root}/tmp/puma/pid"
  state_path "#{root}/tmp/puma/state"
  rackup "#{root}/config.ru"
end

threads 4, 8

activate_control_app
