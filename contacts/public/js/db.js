function log(data) {
    console.log(data);
}

var db = new Dexie('orghub');

db.version(10).stores({
    groups: '&id,&name,created_at'
});

db.groups.put({
    id: 1,
    name: 'PDX Outreach for Justice',
    description: 'Centralizing tech / outreach resources for community organizing',
    created_at: new Date()
}).then(function(data){ console.log(data)}).catch(function(data){ console.log(data)});

console.log('foo');
