// -----------------------------------------------------------------------------
// Definitions for vuejs
var app = new Vue({
    el: '#app',
    data: {
        groupName: '',
        groupDescription: '',
        groups: []
    },
    methods: {
        groupsCreate: function(csrf) {
            var data = { name: this.groupName,
                         description: this.groupDescription }


            axios.post('/groups/create', data, { headers: {
                'Content-Type': 'application/json',
                'X-CSRF-Token': csrf }}).then(
                function(response) {
                    console.log(response);
                }).catch(function(err) { console.log(err) });
            this.groups.push(data);
            this.groupName = this.groupDescription = '';
        }
    },
    mounted() {
        var self = this;
        fetch('/groups').
            then((resp) => resp.json()).
            then(function(groups_json) {
                self.groups =  groups_json;
            }).catch(err => { throw err });
    }
})
