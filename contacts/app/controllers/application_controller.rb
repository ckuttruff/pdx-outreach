require 'encrypted_cookie'
require 'haml'
require 'json'
require 'rack/protection'
require 'securerandom'
require 'sinatra/activerecord'
require 'sinatra/base'

# application_controller.rb
class ApplicationController < Sinatra::Base
  # foo
  Dir.glob('app/{helpers,controllers,models}/*.rb').each { |f| require_relative "../../#{f}" }
  PUBLIC_ROUTES = ['/', '/login']

  helpers ApplicationHelper

  # set folder for templates to ../views, but make the path absolute
  set :views, File.expand_path('../../views', __FILE__)
  set :root, File.expand_path('../../..', __FILE__)
  set :server, :puma

  # TODO: configure logging
  configure :production, :development do
    enable :logging
  end


  use Rack::Session::EncryptedCookie, secret: ENV.fetch('SESSION_SECRET') { SecureRandom.hex(64) }
  use Rack::Protection
  use Rack::Protection::AuthenticityToken
  use Rack::Protection::StrictTransport

  before do
    #dev_mode = (ApplicationController.environment == :development)
    # unless(logged_in? || PUBLIC_ROUTES.include?(request.path_info))
    #   flash_fail('Please login to proceed.')
    #   redirect '/login'
    # end
  end

  get '/' do
    content_type 'text/html'
    @page_title = 'Home'
    haml :index
  end

end
