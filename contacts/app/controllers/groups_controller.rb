class GroupsController < ApplicationController

  before do
    @page_title = 'Groups'
    content_type 'application/json'
  end

  get '/groups' do
    @page_subtitle = 'Group lists'
    # TODO: scope this to @current_user
    @groups = Group.order(:name)
    @groups.to_json
  end

  post '/groups/create' do
    h = JSON.parse(request.env["rack.input"].read)
    Group.create(h)
  end

  get '/group/:id/delete' do
    flash_success('Group successfully deleted')
    Group.destroy(params[:id])
    redirect '/groups'
  end

end
