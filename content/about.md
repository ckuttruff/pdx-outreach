+++
date = "2014-11-08T16:42:18+04:00"
draft = false
title = "About"
slug = "about"

+++

PDX Outreach for Justice helps coordinate outreach efforts for community organizing work.  We support groups, coalitions and individuals with printing, tech tools, canvassing, and other resources.  We are firmly committed to fighting racism, colonialism, imperialism, capitalism, patriarchy and all forms of oppression.  We strive to build true solidarity and coalitions working towards collective liberation and global justice through principled community engagement and direct support.

-----------
