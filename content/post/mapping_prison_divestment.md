+++
categories = []
date = "2017-11-04T00:49:11-07:00"
author = "Christopher Kuttruff"
description = ""
tags = ["prison divestment", "programming", "enlace"]
title = "Mapping Prison Divestment Campaigns"
+++

Enlace has been doing some stellar organizing around prison divestment.  A friend and comrade of mine reached out about mapping out information they've been collecting about divestment campaigns and victories (city, divestment amount, background information, types of campaigns, etc).  We're def not front-end ninjas here at PDXO4J, but we tried our best to present this info in an accessible way.

Here is our process for how we developed this tool.

#### The map
Live link: [https://pdxo4j.org/prison_divest_map.html](https://pdxo4j.org/prison_divest_map.html)

![Prison Divestment Map](/images/posts/prison_divest_map.png)

#### Process

* We decided on a really nice javascript mapping API called [Leaflet](http://leafletjs.com/)
* For latitude / longitude coordinates: [https://www.latlong.net/](https://www.latlong.net/)
  - Note: I'm sure there's a nice API to grab this in a more automated way than I did
* After massaging the data a bit, we ended up with [the following code](https://github.com/ckuttruff/prison_map)

Hoping to improve some elements of this, but I'm pretty excited about how easy it was to get a nice visualization of some really exciting data!  Looking forward to using this for other projects and creating more flexible ways to import / manage / display data that folks want to disseminate.
